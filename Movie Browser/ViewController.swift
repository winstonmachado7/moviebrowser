//
//  ViewController.swift
//  Movie Browser
//
//  Created by Winston Machado on 24/10/18.
//  Copyright © 2018 Winston Machado. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate{
    
    var resarr = [Any]()

    var resarrf = NSMutableArray()

    var resarr2 = [Any]()
    
     var resarrf2 = NSMutableArray()
    
    var btn1 = UISearchBar()
    
    @IBOutlet weak var moviedata: UICollectionView!
    
    @IBOutlet weak var searchData: UITableView!
    
    @IBOutlet weak var popularbtno: UIButton!
    
    @IBOutlet weak var ratingbtno: UIButton!

    var timer = Timer()
    var isGettingData = Bool()
    var temparr = [Any]()
    
    var pager : Int = 1
    
    var pager2: Int = 1
    
    var sortby:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchData.isHidden = true
        
        MovieListing()

        btn1.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width , height: 30)
        
        btn1.placeholder = "search"
        
        btn1.showsCancelButton = false;
        btn1.delegate = self
        btn1.searchBarStyle = .minimal
        btn1.tintColor = UIColor.white
        btn1.barTintColor = UIColor.clear
        
        btn1.backgroundColor = UIColor.clear
        
        for subView in btn1.subviews
        {
            for subView1 in subView.subviews
            {
                
                if subView1 .isKind(of: UITextField.self) {
                    
                    let textfield = subView1 as! UITextField;
                    
                    textfield.textColor = UIColor.white;
                    
                    subView1.backgroundColor = UIColor.clear;
                    
                    textfield.font = UIFont(name: "HelveticaNeue", size: 16.9)
                    
                    let textFieldInsideSearchBarLabel = textfield.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideSearchBarLabel?.textColor = UIColor(hexFromString: "#FFFFFF", alpha: 1.0)
                    
                    let clearButton = textfield.value(forKey: "clearButton") as! UIButton
                    clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
                    clearButton.tintColor = UIColor(hexFromString: "#FFFFFF", alpha: 1.0)
                    
                    let glassIconView = textfield.leftView as? UIImageView
                    glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
                    glassIconView?.tintColor = UIColor(hexFromString: "#FFFFFF", alpha: 1.0)
                    
                }
                
            }
            
        }
        
        
        let searchTextField: UITextField? = btn1.value(forKey: "searchField") as? UITextField
        if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributes: [NSAttributedString.Key : Any] =
                [.foregroundColor: UIColor.white]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributes)
            
        }
        
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setTitle("Movie Browser", for: .normal)
        
        btn2.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20)
        
        btn2.titleLabel?.textAlignment = .left
        
        btn2.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setLeftBarButtonItems([item2,item1], animated: true)
      
        popularbtno.layer.cornerRadius = 20
        ratingbtno.layer.cornerRadius = 20
        
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resarrf.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieListingCVC", for: indexPath) as! MovieListingCVC
        let moviedict:NSDictionary = resarrf[indexPath.item] as! NSDictionary
        cell.movietitle.text = moviedict.value(forKey: "title") as? String
        if ((moviedict.value(forKey: "poster_path") as? String) != nil)
        {
            let imgstr2:String = moviedict.value(forKey: "poster_path") as! String
            let imageURL = NSURL(string: BaseImgurl + imgstr2)
            cell.movieposterimg.sd_setImage(with: imageURL as URL?)
        }
        else{
        
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: (collectionView.frame.size.width-10)/2 , height: 180)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailsVC") as? MovieDetailsVC
        let moviedict:NSDictionary = resarrf[indexPath.item] as! NSDictionary
        NewVC?.movieimg = moviedict.value(forKey: "backdrop_path") as? String
        NewVC?.movieimg2 = moviedict.value(forKey: "poster_path") as? String
        
        NewVC?.mtitle = moviedict.value(forKey: "title") as? String
        NewVC?.mdate = moviedict.value(forKey: "release_date") as? String
        NewVC?.mrating = moviedict.value(forKey: "vote_average") as? NSNumber
        NewVC?.mplot = moviedict.value(forKey: "overview") as? String
        
        
        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resarrf2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVC", for: indexPath) as! SearchTVC
        let moviedict:NSDictionary = resarrf2[indexPath.item] as! NSDictionary
        cell.searchlbl.text = moviedict.value(forKey: "title") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let NewVC = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailsVC") as? MovieDetailsVC
        let moviedict:NSDictionary = resarrf2[indexPath.item] as! NSDictionary

        NewVC?.movieimg = moviedict.value(forKey: "backdrop_path") as? String
       
        NewVC?.movieimg2 = moviedict.value(forKey: "poster_path") as? String
        
        NewVC?.mtitle = moviedict.value(forKey: "title") as? String
        NewVC?.mdate = moviedict.value(forKey: "release_date") as? String
        NewVC?.mrating = moviedict.value(forKey: "vote_average") as? NSNumber
        NewVC?.mplot = moviedict.value(forKey: "overview") as? String
        
        
        self.navigationController?.pushViewController(NewVC!, animated: true)
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        btn1.showsCancelButton = true
    }
    
   
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        btn1.resignFirstResponder()
        btn1.text = ""
        searchData.isHidden = true
        btn1.showsCancelButton = false
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        MovieSearch()
        searchData.isHidden = false
    }
    
    @IBAction func sortbypopular(_ sender: UIButton) {
        sortby = "popularity.asc"
        resarr.removeAll()
        resarrf.removeAllObjects()
        
        MovieSorting()
    }
    
    
    @IBAction func sortbyrating(_ sender: UIButton) {
        sortby = "vote_average.asc"
        resarr.removeAll()
        resarrf.removeAllObjects()
        MovieSorting()
    }
    
    func MovieListing()
    {
       
        let parameters: Parameters = ["api_key":"c39b252086d51efc8b153c24a31ca620",
                                      "page":pager]
        
        ApiManager().requestApiWithDataType( methodType: GET, urlString: Listing_URL(),parameters: parameters as [String : AnyObject]) { (response,isSuccess, error) in
            if isSuccess!{
                
                let resdict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                self.temparr = resdict.value(forKey: "results") as! [Any]
                
                self.resarr = NSMutableArray(array: self.temparr) as! [Any]
                
                self.resarrf.addObjects(from:self.resarr as! [Any])
                if self.resarr.count > 10 {
                    self.pager = self.pager + 1
                    
                    self.isGettingData = false
                }
                else {
                    
                }
                
                self.moviedata.reloadData()
                
            }
        }
        
    }
    
    
    func MovieSearch()
    {
        
        let parameters: Parameters = ["api_key":"c39b252086d51efc8b153c24a31ca620",
                                      "query":btn1.text!,
                                      "page":pager2]
        
        ApiManager().requestApiWithDataType( methodType: GET, urlString: Search_URL(),parameters: parameters as [String : AnyObject]) { (response,isSuccess, error) in
            if isSuccess!{
                
                let resdict2:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                
                //self.resarr2 = resdict2.value(forKey: "results") as! NSArray
                
                self.temparr = resdict2.value(forKey: "results") as! [Any]
                
                self.resarr2 = NSMutableArray(array: self.temparr) as! [Any]
                
                self.resarrf2.addObjects(from:self.resarr2 as! [Any])
                if self.resarr2.count > 20 {
                    self.pager2 = self.pager2 + 1
                    
                    self.isGettingData = false
                }
                else {
                    
                }
                
                self.searchData.reloadData()
                
            }
        }
        
    }
    
    
    
    func MovieSorting()
    {

        let parameters: Parameters = ["api_key":"c39b252086d51efc8b153c24a31ca620",
                                      "sort_by":sortby]

        ApiManager().requestApiWithDataType( methodType: GET, urlString: Listing_URL(),parameters: parameters as [String : AnyObject]) { (response,isSuccess, error) in
            if isSuccess!{

                let resdict:NSDictionary = convertStringToDictionary(json: response as! String)! as NSDictionary
                self.resarrf = resdict.value(forKey: "results") as! NSMutableArray

                self.moviedata.reloadData()

            }
        }

    }
    
    
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if moviedata.contentOffset.y < 0 {
            
            return
            
        }
            
        else if moviedata.contentOffset.y >= (moviedata.contentSize.height - moviedata.bounds.size.height) {
            
            
            
            timer = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: false)
            
        }
        
        
        if searchData.contentOffset.y < 0 {
            
            return
            
        }
            
        else if searchData.contentOffset.y >= (searchData.contentSize.height - searchData.bounds.size.height) {
            
            
            
            timer = Timer.scheduledTimer(timeInterval: 0.9, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: false)
            
        }
        
        
        
    }
    
    @objc func timerCallback() {
        
        timer.invalidate()
        
        if isGettingData == false && temparr.count > 10 {
            
            resarr.removeAll()
            
            MovieListing()
            
        }
        
    }
    
    
    @objc func timerCallback2() {
        
        timer.invalidate()
        
        if isGettingData == false && temparr.count > 20 {
            
            resarr2.removeAll()
            
            MovieSearch()
            
        }
        
    }
    
    
}


extension UIColor {
    convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
