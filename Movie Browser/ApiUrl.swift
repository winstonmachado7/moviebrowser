//
//  ApiUrl.swift
//  Movie Browser
//
//  Created by Winston Machado on 24/10/18.
//  Copyright © 2018 Winston Machado. All rights reserved.
//

import Foundation
import UIKit

var Test_IP = "https://api.themoviedb.org/3"


func Listing_URL() -> String {
    return Test_IP + "/discover/movie";
}

func Search_URL() -> String {
    return Test_IP + "/search/movie";
}

