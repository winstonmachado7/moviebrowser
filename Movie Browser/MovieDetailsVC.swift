//
//  MovieDetailsVC.swift
//  Movie Browser
//
//  Created by Welborn Machado on 25/10/18.
//  Copyright © 2018 Winston Machado. All rights reserved.
//

import UIKit
import SDWebImage

class MovieDetailsVC: UIViewController {

    var movieimg:String!
    
    var movieimg2:String!
    
    var mtitle:String!
    
    var mdate:String!
    
    var mrating:NSNumber!
    
    var mplot:String!
    
    @IBOutlet weak var moviedetailimg: UIImageView!
    
    @IBOutlet weak var moviedetailbg: UIImageView!
    
    @IBOutlet weak var movietitle: UILabel!
    
    @IBOutlet weak var movierdate: UILabel!
    
    @IBOutlet weak var movieratings: UILabel!
    
    @IBOutlet weak var movieplot: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if movieimg == nil
        {
         
        }
        else{
        let imageURL = NSURL(string: BaseImgurl + movieimg)
        moviedetailbg.sd_setImage(with: imageURL as URL?)
        }
        
        if movieimg2 == nil
        {
            
        }
        else{
        let imageURL2 = NSURL(string: BaseImgurl + movieimg2)
        moviedetailimg.sd_setImage(with: imageURL2 as URL?)
        }
        
        movietitle.text = mtitle
        movierdate.text = mdate
        movieratings.text = String(describing: mrating!)
        movieplot.text = mplot
        
    }
    

    

}
