//
//  Constants.swift
//  Movie Browser
//
//  Created by Winston Machado on 24/10/18.
//  Copyright © 2018 Winston Machado. All rights reserved.
//

import Foundation
import UIKit

let GET:String = "GET"
let POST:String = "POST"

let HGET:String = "HGET"
let HPOST:String = "HPOST"

let BaseImgurl:String = "https://image.tmdb.org/t/p/w500"
