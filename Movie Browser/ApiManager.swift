//
//  ApiManager.swift
//  Movie Browser
//
//  Created by Winston Machado on 24/10/18.
//  Copyright © 2018 Winston Machado. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager: NSObject {
    
    func requestApiWithDataType(methodType:String,urlString:String,parameters:[String:AnyObject]? = nil,completionHandeler: @escaping (Any?,Bool?,NSError?) -> Void){
    
    let headers = ["Content-Type": "application/json"]
        
       // let login_token = getUserdefaultDataForKey(Key:"Login_Token") as! String ?? ""
    
       // let udid = getUserdefaultDataForKey(Key:"UDID") as! String ?? ""

    //let headers  = ["ApiToken" : login_token,
                      //  "DID" : udid]
        
      //  print(headers)
        
        switch methodType
        {
        case GET:
            Alamofire.request(urlString, method: .get, parameters: parameters, encoding:URLEncoding.default)
                .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                    print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseString { response in
                    debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200
                        {
                            
                            completionHandeler(JSON,response.result.isSuccess ,nil)
                        }
                        else
                        {
                            completionHandeler(JSON,false,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,response.result.isSuccess ,error as NSError)
                    }
            }
            break
        case POST:
            Alamofire.request(urlString, method:.post, parameters: parameters, encoding: JSONEncoding.default).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseString { response in
                    debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200
                        {
                            
                            completionHandeler(JSON,response.result.isSuccess ,nil)
                        }
                        else
                        {
                            completionHandeler(JSON,false,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,response.result.isSuccess ,error as NSError)
                    }
            }
        case HGET:
            Alamofire.request(urlString, method: .get, parameters: parameters, encoding: JSONEncoding.default,headers:headers)
                .downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                    print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseString { response in
                    debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200
                        {

                            completionHandeler(JSON,response.result.isSuccess ,nil)
                        }
                        else
                        {
                            completionHandeler(JSON,false,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,response.result.isSuccess ,error as NSError)
                    }
            }
        case HPOST:
            Alamofire.request(urlString, method:.post, parameters: parameters, encoding: JSONEncoding.default,headers:headers).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
                print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseString { response in
                    debugPrint(response)
                    switch response.result
                    {
                    case .success(let JSON) :
                        if response.response?.statusCode == 200
                        {

                            completionHandeler(JSON,response.result.isSuccess ,nil)
                        }
                        else
                        {
                            completionHandeler(JSON,false,nil)
                        }
                    case .failure(let error):
                        completionHandeler(nil,response.result.isSuccess ,error as NSError)
                    }
            }
            
            break
        default :
            break
        }
    }
}
