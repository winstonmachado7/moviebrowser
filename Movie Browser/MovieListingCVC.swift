//
//  MovieListingCVC.swift
//  Movie Browser
//
//  Created by Welborn Machado on 24/10/18.
//  Copyright © 2018 Winston Machado. All rights reserved.
//

import UIKit

class MovieListingCVC: UICollectionViewCell {
    
    @IBOutlet weak var movieposterimg: UIImageView!
    
    @IBOutlet weak var movietitle: UILabel!
    
}
